#!/usr/bin/bash
audio_output_status=$(cat /proc/asound/card0/pcm0p/sub0/status) # <1>

if [ "$audio_output_status"  == "closed" ]; then
    echo "$audio_output_status: Playing sound."
    play -n -n --combine merge synth '0:05' brownnoise band -n 20000 50 tremolo 50 1 gain +25 # <2>

else
    echo "Audio playback detected, not playing keepalive"
    
fi